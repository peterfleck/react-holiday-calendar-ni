import { useEffect, useState } from "react";
import Header from "./components/Header.jsx";
import Loader from "./components/Loader.jsx";
import Calendar from "./components/Calendar.jsx";
import fetchBankHolidays from "./utilities/fetchBankHolidays.js";

export default function App() {
  const [bankHolidays, setBankHolidays] = useState([]);
  const [loaded, setLoaded] = useState(false);

  const loadBankHolidays = async () => {
    setLoaded(false);
    setBankHolidays(await fetchBankHolidays("northern-ireland"));
    setLoaded(true);
  };

  useEffect(() => {
    loadBankHolidays();
  }, []);

  return (
    <div className="container mt-4">
      <Header country="Northern Ireland"/>
      {loaded === false ? <Loader /> : <Calendar events={bankHolidays} />}
    </div>
  );
}
