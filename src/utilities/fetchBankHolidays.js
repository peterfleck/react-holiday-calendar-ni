import Ajv from "ajv";
import schema from "../openapi.json"

export default async function fetchBankHolidays(country) {
  const countrySchema = new Ajv().compile(schema.components.schemas.country);
  const valid = countrySchema(country);

  if (!valid) {
    console.log(countrySchema.errors);
    return countrySchema.errors;
  } else if (valid) {
    const bankHolidays = await fetch(`https://www.gov.uk/bank-holidays.json`)
      .then((response) => {
        if (!response.ok) {
          throw new Error("Network response was not ok");
        }
        return response.json();
      })
      .catch((error) => {
        console.error(
          "There has been a problem with fetching the bank holidays.",
          error
        );
      });

    return bankHolidays[country].events;
  }
}
