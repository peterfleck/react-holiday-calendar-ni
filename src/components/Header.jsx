export default function Header({country}) {
  return (
    <h1 className="bg-success p-2 text-white text-center">
       {country} Bank Holidays
    </h1>
  );
}
