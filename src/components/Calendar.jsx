import FullCalendar from "@fullcalendar/react";
import dayGridPlugin from "@fullcalendar/daygrid";

export default function Calendar({ events }) {
  return (
    <FullCalendar
      plugins={[dayGridPlugin]}
      initialView="dayGridMonth"
      headerToolbar={{
        start: "dayGridMonth,dayGridWeek,dayGridDay",
        center: "title",
        end: "today prev,next",
      }}
      events={events}
    />
  );
}
